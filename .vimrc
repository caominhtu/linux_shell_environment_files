" reference: http://dougblack.io/words/a-good-vimrc.html

colorscheme desert
syntax on    " enable syntax hilighting
set tabstop=4    " number of visual spaces per TAB
set softtabstop=4    " number of spaces in tab when editing
set expandtab    " tabs are spaces
set showcmd    " show command in bottom bar (status bar)
set cursorline    " highlight current line
" set current line highlight style (different from desert scheme of underline current line)
highlight CursorLine term=bold cterm=bold guibg=Grey40
" filetype indent on    " load filetype-specific indent files
set wildmenu    " visual autocomplete for command menu
set lazyredraw    " redraw only when we need to
set showmatch    " highlight matching parentheses [{()}]
set incsearch    " search as chracters are entered
set hlsearch    " highlight search matches
" turn off search hightlight with keyboard shortcut: ,<space>
nnoremap ,<space> :nohlsearch<CR>
set foldenable    " enable folding
set foldlevelstart=10   " open most folds by default
set foldnestmax=10      " 10 nested fold max
" space open/closes folds
nnoremap <space> za
" set foldmethod=syntax   " fold based on syntax
set foldmethod=indent   " fold based on indent level

" " move vertically by visual line (instead of 'real' line)
" nnoremap j gj
" nnoremap k gk

" highlight last inserted text
nnoremap gV `[v`]

" Allow saving of files as sudo when I forgot to start vim using sudo.
cmap w!! w !sudo tee > /dev/null %
